package uz.test.desktop;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {

    BorderPane borderPane = new BorderPane();

    @Override
    public void start(Stage stage) throws Exception{
        loadView(new Locale("uz","UZ"));
        borderPane.setTop(createComboBox());
        stage.setScene(new Scene(borderPane));
        stage.setTitle("Internationalization");
        stage.show();
    }

    private ComboBox<Locale> createComboBox() {
        ComboBox<Locale> comboBox = new ComboBox<>();

        ObservableList<Locale> options = FXCollections.observableArrayList(new Locale("uz","UZ"), new Locale("ru","RU"));
        comboBox.setItems(options);
        comboBox.setConverter(new StringConverter<Locale>() {
            @Override
            public String toString(Locale object) {
                return object.getDisplayLanguage();
            }

            @Override
            public Locale fromString(String string) {
                return null;
            }
        });
        comboBox.setCellFactory(p -> new LanguageListCell());
        comboBox.getSelectionModel().selectFirst();

        comboBox.setOnAction(event -> loadView(comboBox.getSelectionModel().getSelectedItem()));
        return comboBox;
    }

    private void loadView(Locale locale) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setResources(ResourceBundle.getBundle("lang/lang", locale));

            Pane pane = (BorderPane) fxmlLoader.load(this.getClass().getResource("/uz/test/desktop/fxml/sample.fxml").openStream());
            borderPane.setCenter(pane);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    class LanguageListCell extends ListCell<Locale> {
        @Override protected void updateItem(Locale item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                setText(item.getDisplayLanguage());
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

