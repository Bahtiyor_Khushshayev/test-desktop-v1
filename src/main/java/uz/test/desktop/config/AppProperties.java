package uz.test.desktop.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppProperties {

    private final Properties configProp = new Properties();

    private AppProperties() {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties");

        try {
            configProp.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class LazyHolder {
        private static final AppProperties INSTANCE = new AppProperties();
    }

    public static AppProperties getInstance() {
        return LazyHolder.INSTANCE;
    }

    public String getProperty(String key) {
        return configProp.getProperty(key);
    }

    public boolean containsKey(String key) {
        return configProp.containsKey(key);
    }
}
